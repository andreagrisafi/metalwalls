   ! Tests minimum image displacement when 1 is at the center of the box
   @test
   subroutine test_minimum_image_displacement_center()
      use pfunit_mod
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_minimum_image_displacement => minimum_image_displacement_2DPBC
      use MW_kinds, only: wp
      implicit none
      ! Parameter

      real(wp), parameter :: tol = 1.0e-15

      type(MW_box_t) :: box
      real(wp), parameter :: a = 2.0_wp
      real(wp), parameter :: b = 2.0_wp
      real(wp), parameter :: c = 2.0_wp

      integer :: nx, ny, i2, j2
      integer, parameter :: nx_start = -1
      integer, parameter :: nx_end = 1
      integer, parameter :: ny_start = 0
      integer, parameter :: ny_end = 0

      real(wp) :: xyz_1(3), xyz_2(3,3,3), dr(3,3,3), drnorm2(3,3)
      real(wp) :: xyz_2min(3,3,3), drmin(3,3,3), drminnorm2(3,3)

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      ! Test 1
      ! ------
      ! xyz_1 in the center of the box
      xyz_1(1) =  0.5_wp * a
      xyz_1(2) =  0.5_wp * b
      xyz_1(3) =  0.5_wp * c

      do i2 = 1, 3
         do j2 = 1, 3
            xyz_2min(1,j2,i2) = i2 * a / 4.0_wp
            xyz_2min(2,j2,i2) = j2 * b / 4.0_wp
            xyz_2min(3,j2,i2) = 0.5_wp * c

            drmin(1,j2,i2) = xyz_2min(1,j2,i2) - xyz_1(1)
            drmin(2,j2,i2) = xyz_2min(2,j2,i2) - xyz_1(2)
            drmin(3,j2,i2) = xyz_2min(3,j2,i2) - xyz_1(3)

            drminnorm2(j2,i2) = dot_product(drmin(:,j2,i2), drmin(:,j2,i2))
         end do
      end do

      dr(:,:,:) = 0.0_wp
      drnorm2(:,:) = 0.0_wp

      do nx = nx_start, nx_end
         do ny = ny_start, ny_end
            do i2 = 1, 3
               do j2 = 1, 3

                  xyz_2(1,j2,i2) = xyz_2min(1,j2,i2) + nx * a
                  xyz_2(2,j2,i2) = xyz_2min(2,j2,i2) + ny * b
                  xyz_2(3,j2,i2) = xyz_2min(3,j2,i2)

                  call MW_box_minimum_image_displacement(box%length(1), box%length(2), box%length(3),&
                        xyz_1(1), xyz_1(2), xyz_1(3), xyz_2(1,j2,i2), &
                        xyz_2(2,j2,i2), xyz_2(3,j2,i2), dr(1,j2,i2), dr(2,j2,i2), dr(3,j2,i2), drnorm2(j2,i2))
               end do
            end do

            ! Tests
            do i2 = 1, 3
               do j2 = 1, 3
                  @assertEqual(drmin(:,j2,i2), dr(:,j2,i2), tol)
               end do
            end do
         end do
      end do

      ! Clean up
      ! --------
      call MW_box_void_type(box)
   end subroutine test_minimum_image_displacement_center

   ! ========================================================================
   ! Tests minimum image displacement when 1 is not at the center of the bo
   @test
   subroutine test_minimum_image_displacement()
      use pfunit_mod
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_minimum_image_displacement => minimum_image_displacement_2DPBC
      use MW_kinds, only: wp
      implicit none
      real(wp), parameter :: tol = 1.0e-15

      type(MW_box_t) :: box
      real(wp), parameter :: a = 1.0_wp
      real(wp), parameter :: b = 2.0_wp
      real(wp), parameter :: c = 3.0_wp

      integer :: nx, ny, i1, j1, i2, j2
      integer, parameter :: nx_start = -2
      integer, parameter :: nx_end = +2
      integer, parameter :: ny_start = -2
      integer, parameter :: ny_end = +2

      real(wp) :: xyz_1(3), xyz_2(3,4,4), dr(3,4,4), drnorm2(4,4)
      real(wp) :: xyz_2min(3,4,4), drmin(3,4,4), drminnorm2(4,4)

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      xyz_1(:) = 0.0_wp
      xyz_2(:,:,:) = 0.0_wp
      dr(:,:,:) = 0.0_wp
      drnorm2(:,:) = 0.0_wp

      xyz_2min(:,:,:) = 0.0_wp
      drmin(:,:,:) = 0.0_wp
      drminnorm2(:,:) = 0.0_wp



      do i1 = 1, 4
         do j1 = 1, 4

            xyz_1(1) =  (i1-1) * a / 4.0_wp
            xyz_1(2) =  (j1-1) * b / 4.0_wp
            xyz_1(3) =  0.5_wp * c

            do i2 = 1, 4
               do j2 = 1, 4
                  xyz_2min(1,j2,i2) = (i2-1) * a / 4.0_wp - a / 2.0_wp + xyz_1(1)
                  xyz_2min(2,j2,i2) = (j2-1) * b / 4.0_wp - b / 2.0_wp + xyz_1(2)
                  xyz_2min(3,j2,i2) = 0.5_wp * c

                  drmin(1,j2,i2) = xyz_2min(1,j2,i2) - xyz_1(1)
                  drmin(2,j2,i2) = xyz_2min(2,j2,i2) - xyz_1(2)
                  drmin(3,j2,i2) = xyz_2min(3,j2,i2) - xyz_1(3)

                  drminnorm2(j2,i2) = dot_product(drmin(:,j2,i2), drmin(:,j2,i2))
               end do
            end do

            do nx = nx_start, nx_end
               do ny = ny_start, ny_end
                  do i2 = 1, 4
                     do j2 = 1, 4

                        xyz_2(1,j2,i2) = xyz_2min(1,j2,i2) + nx * a
                        xyz_2(2,j2,i2) = xyz_2min(2,j2,i2) + ny * b
                        xyz_2(3,j2,i2) = xyz_2min(3,j2,i2)

                        call MW_box_minimum_image_displacement(box%length(1), box%length(2), box%length(3),&
                              xyz_1(1), xyz_1(2), xyz_1(3), xyz_2(1,j2,i2), &
                              xyz_2(2,j2,i2), xyz_2(3,j2,i2), &
                              dr(1,j2,i2), dr(2,j2,i2), dr(3,j2,i2), drnorm2(j2,i2))
                     end do
                  end do

                  ! Tests
                  do i2 = 1, 4
                     do j2 = 1, 4
                        @assertEqual(drmin(:,j2,i2), dr(:,j2,i2), tol)
                     end do
                  end do
               end do
            end do
         end do
      end do

      ! Clean up
      ! --------
      call MW_box_void_type(box)
   end subroutine test_minimum_image_displacement

   ! ========================================================================
   ! Tests minimum image distance when 1 is not at the center of the box
   @test
   subroutine test_minimum_image_distance()
      use pfunit_mod
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_minimum_image_distance => minimum_image_distance_2DPBC
      use MW_kinds, only: wp
      implicit none
      ! Parameter
      real(wp), parameter :: tol = 1.0e-15

      type(MW_box_t) :: box
      real(wp), parameter :: a = 1.0_wp
      real(wp), parameter :: b = 2.0_wp
      real(wp), parameter :: c = 3.0_wp

      integer :: nx, ny, i1, j1, i2, j2
      integer, parameter :: nx_start = -2
      integer, parameter :: nx_end = +2
      integer, parameter :: ny_start = -2
      integer, parameter :: ny_end = +2

      real(wp) :: xyz_1(3), xyz_2(3,4,4), drnorm2(4,4)
      real(wp) :: xyz_2min(3,4,4), drmin(3,4,4), drminnorm2(4,4)

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      xyz_1(:) = 0.0_wp
      xyz_2(:,:,:) = 0.0_wp
      drnorm2(:,:) = 0.0_wp
      xyz_2min(:,:,:) = 0.0_wp
      drmin(:,:,:) = 0.0_wp
      drminnorm2(:,:) = 0.0_wp

      do i1 = 1, 4
         do j1 = 1, 4

            xyz_1(1) =  (i1-1) * a / 4.0_wp
            xyz_1(2) =  (j1-1) * b / 4.0_wp
            xyz_1(3) =  0.5_wp * c

            do i2 = 1, 4
               do j2 = 1, 4
                  xyz_2min(1,j2,i2) = (i2-1) * a / 4.0_wp - a / 2.0_wp + xyz_1(1)
                  xyz_2min(2,j2,i2) = (j2-1) * b / 4.0_wp - b / 2.0_wp + xyz_1(2)
                  xyz_2min(3,j2,i2) = 0.5_wp * c

                  drmin(1,j2,i2) = xyz_2min(1,j2,i2) - xyz_1(1)
                  drmin(2,j2,i2) = xyz_2min(2,j2,i2) - xyz_1(2)
                  drmin(3,j2,i2) = xyz_2min(3,j2,i2) - xyz_1(3)

                  drminnorm2(j2,i2) = dot_product(drmin(:,j2,i2), drmin(:,j2,i2))
               end do
            end do

            do nx = nx_start, nx_end
               do ny = ny_start, ny_end
                  do i2 = 1, 4
                     do j2 = 1, 4

                        xyz_2(1,j2,i2) = xyz_2min(1,j2,i2) + nx * a
                        xyz_2(2,j2,i2) = xyz_2min(2,j2,i2) + ny * b
                        xyz_2(3,j2,i2) = xyz_2min(3,j2,i2)

                        call MW_box_minimum_image_distance(box%length(1), box%length(2), box%length(3),&
                              xyz_1(1), xyz_1(2), xyz_1(3), &
                              xyz_2(1,j2,i2), xyz_2(2,j2,i2), xyz_2(3,j2,i2), &
                              drnorm2(j2,i2))
                     end do
                  end do

                  ! Tests
                  do i2 = 1, 4
                     do j2 = 1, 4
                        @assertEqual(drminnorm2(j2,i2), drnorm2(j2,i2), tol)
                     end do
                  end do
               end do
            end do
         end do
      end do

      ! Clean up
      ! --------
      call MW_box_void_type(box)
   end subroutine test_minimum_image_distance

   ! ========================================================================
   ! Tests outlier fixing
   @test
   subroutine test_fix_outliers()
      use pfunit_mod
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type, &
            MW_box_void_type => void_type, &
            MW_box_fix_outliers => fix_outliers
      use MW_kinds, only: wp
      implicit none
      ! Parameter
      real(wp), parameter :: tol = 1.0e-15

      type(MW_box_t) :: box
      real(wp), parameter :: a = 1.0_wp
      real(wp), parameter :: b = 2.0_wp
      real(wp), parameter :: c = 3.0_wp

      integer :: nx, ny, i, j
      integer, parameter :: nx_start = -2
      integer, parameter :: nx_end = +2
      integer, parameter :: ny_start = -2
      integer, parameter :: ny_end = +2

      real(wp) :: xyz_1(3,4,4)
      real(wp) :: xyz_2(3,4,4)

      ! Setup
      ! -----
      call MW_box_define_type(box, a, b, c)

      do i = 1, 4
         do j = 1, 4

            xyz_1(1, j, i) =  (i-1) * a / 4.0_wp
            xyz_1(2, j, i) =  (j-1) * b / 4.0_wp
            xyz_1(3, j, i) =  0.5_wp * c



            do nx = nx_start, nx_end
               do ny = ny_start, ny_end
                  xyz_2(1,j,i) = xyz_1(1,j,i) + nx * a
                  xyz_2(2,j,i) = xyz_1(2,j,i) + ny * b
                  xyz_2(3,j,i) = xyz_1(3,j,i)
                  call MW_box_fix_outliers(box, 2, &
                        xyz_2(1,j,i), xyz_2(2,j,i), xyz_2(3,j,i))
               end do
            end do

            ! Tests
            @assertEqual(xyz_1(:,j,i), xyz_2(:,j,i), tol)
         end do
      end do

      ! Clean up
      ! --------
      call MW_box_void_type(box)
   end subroutine test_fix_outliers


