# Compilation options
F90 := mpif90
F90FLAGS := -O0 -g -fcheck=all
FPPFLAGS := -cpp -DMW_CI -DMW_USE_PLUMED
LDFLAGS := -llapack
J := -J

# Path to pFUnit (Unit testing Framework)
PFUNIT := $(PFUNIT)
