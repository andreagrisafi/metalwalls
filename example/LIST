# Folder name                                 Description                                                      Estimated Time (4-CPUs laptop)
# -------------------------------------------------------------------------------------------------------------------------------------------
blue-energy-cdc                               2D porous carbon CDC with aqueous NaCl at constant               ~ 60 min
                                              potential solved with matrix inversion

electric-field                                3D box with a model electrode and aqueous NaCl electrolyte       < 1 min
                                              with finite electric field, solved using conjugate gradient

EMIM_BF4_graphene                             2D planar carbon with ionic liquid at constant Pz (piston)       ~ 15 min
                                              and constant potential solved with conjugate gradient

EMIM_TFSI_bulk_no_polarizability              3D ionic liquid                                                  < 1 min

EMIM_TFSI_bulk_polarizability                 3D polarizable ionic liquid                                      < 1 min

LiCl-aluminum                                 2D planar aluminum electrodes with molten salt                   < 1 min
                                              at constant potential solved with MaZe with matrix
                                              inversion

NaCl_PIM_NPT                                  3D cell with polarizable NaCl and NPT ensemble                   < 1 min

nanoporous-graphene                           2D nanoporous planar carbon with ionic liquid,                   ~ 30 min
                                              at constant potential solved with conjugate gradient

plumed                                        2D planar graphene sheets at constant electrode charges          ~ 2 min
                                              with water and a Na+ ion with a PLUMED constraint on
                                              its position

polarizable-water                             3D polarizable water                                             < 1 min

python-piston-mgo                             2D MgO planar electrodes with NaNO3 electrolyte at               ~ 1 min
                                              constant electrode charges using python interface
                                              to impose constant pressure (piston)

python-temperature_ramp                       3D cell with NaCl molten salt, NPT simulation                    < 1 min
                                              using python interface to vary temperature from 800 to 1500 K 

python-thermo_integration_charge              3D water box with an ion: thermodynamic integration for          ~ 10 min
                                              a change in the ion charge using python interface

python-thermo_integration_lj_lj               3D water box with an ion: thermodynamic integration for          ~ 3 min
                                              a change in the ion Lennard-Jones parameters using python
                                              interface

python-thermo_integration_lj_lj_brute_force   same as python-thermo_integration_lj_lj,                         ~ 15 min
                                              without specific optimizations

python-thermo_integration_soft_core           3D water box with an ion: thermodynamic integration for          ~ 30 min
                                              the insertion of the ion with a soft-core LJ potential,
                                              using python interface

steele-argon                                  2D Steele walls with argon fluid,                                < 1 min
                                              at constant Pz (piston)

thomas-fermi-gold-capacitor                   2D planar gold electrodes with Thomas-Fermi model,               ~ 5 min
                                              with a 1M NaCl electrolyte, at constant potential
                                              solved using matrix inversion

tip4p-water                                   3D cell with TIP4P water                                         ~ 2 min

EMIM-TFSI_graphene_polarizability             2D cell planar carbon with ionic liquid at constant potential    ~ 40 min
                                              and polarisability of ions using conjugate-gradient
