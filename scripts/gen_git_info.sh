#/bin/bash
# Generate fortran statements to write the output of the git information

echo '! Automatically generated file with output from git commands output' > ${1}
echo 'write(ounit,'"'"'("MetalWalls release ",a)'"'"') "'${2}'"' >> ${1}
echo 'write(ounit, *)' >> ${1}
